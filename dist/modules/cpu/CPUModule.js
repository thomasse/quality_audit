"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CPUModule = exports.CPUModuleEvents = void 0;
const AbstractPuppeteerJourneyModule_1 = require("../../journey/AbstractPuppeteerJourneyModule");
const AbstractPuppeteerJourney_1 = require("../../journey/AbstractPuppeteerJourney");
const ModuleInterface_1 = require("../ModuleInterface");
exports.CPUModuleEvents = {
    createCPUModule: 'cpu__createCPUModule',
    beforeAnalyse: 'cpu__beforeAnalyse',
    onResult: 'cpu__onResult',
    onBrowserClose: 'cpu__onBrowserClose',
    onBrowserLaunch: 'cpu__onBrowserLaunch',
    onNewPage: 'cpu__onNewPage',
    afterAnalyse: 'cpu__afterAnalyse',
};
class CPUModule extends AbstractPuppeteerJourneyModule_1.AbstractPuppeteerJourneyModule {
    constructor() {
        super(...arguments);
        this.snapshots = [];
        this.currentStep = 0;
        this.currentContext = 0;
        this.hasValue = false;
        this.isPaused = false;
        this.cpuCount = 1;
        this.durationsMetrics = [
            // 'Timestamp',
            // 'AudioHandlers',
            // 'Documents',
            // 'Frames',
            // 'JSEventListeners',
            // 'LayoutObjects',
            // 'MediaKeySessions',
            // 'MediaKeys',
            // 'Nodes',
            // 'Resources',
            // 'ContextLifecycleStateObservers',
            // 'V8PerContextDatas',
            // 'WorkerGlobalScopes',
            // 'UACSSResources',
            // 'RTCPeerConnections',
            // 'ResourceFetchers',
            // 'AdSubframes',
            // 'DetachedScriptStates',
            // 'ArrayBufferContents',
            // 'LayoutCount',
            // 'RecalcStyleCount',
            'LayoutDuration',
            'RecalcStyleDuration',
            'DevToolsCommandDuration',
            'ScriptDuration',
            'V8CompileDuration',
            'TaskDuration',
            'TaskOtherDuration',
            // 'ThreadTime',
            // 'ProcessTime',
            // 'JSHeapUsedSize',
            // 'JSHeapTotalSize',
            // 'FirstMeaningfulPaint',
            // 'DomContentLoaded',
            // 'NavigationStart',
        ];
        this.mainDurationMetrics = [
            'LayoutDuration',
            'RecalcStyleDuration',
            'ScriptDuration',
        ];
    }
    get name() {
        return 'CPU';
    }
    get id() {
        return `cpu`;
    }
    /**
     * {@inheritdoc}
     */
    init(context) {
        var _a, _b, _c, _d, _e;
        this.context = context;
        const os = require('os-utils');
        this.cpuCount = os.cpuCount();
        // Install eco index store.
        (_b = (_a = this.context) === null || _a === void 0 ? void 0 : _a.config.storage) === null || _b === void 0 ? void 0 : _b.installStore('cpu', this.context, {
            url: 'Url',
            context: 'context',
            time: 'Time',
            cpu: 'CPU use average (%)',
        });
        // Install eco index best_practices.
        (_d = (_c = this.context) === null || _c === void 0 ? void 0 : _c.config.storage) === null || _d === void 0 ? void 0 : _d.installStore('cpu_history', this.context, {
            url: 'Url',
            time: 'Time',
            step: 'Step',
            context: 'Context',
            cpu: 'CPU usage (%)',
        });
        // Emit.
        (_e = this.context) === null || _e === void 0 ? void 0 : _e.eventBus.emit(exports.CPUModuleEvents.createCPUModule, { module: this });
    }
    /**
     * {@inheritdoc}
     */
    initEvents(journey) {
        var _a, _b;
        // Init ecoindex data.
        journey.on(AbstractPuppeteerJourney_1.PuppeteerJourneyEvents.JOURNEY_START, (data) => __awaiter(this, void 0, void 0, function* () { return this.startTimer(data); }));
        journey.on(AbstractPuppeteerJourney_1.PuppeteerJourneyEvents.JOURNEY_AFTER_STEP, () => __awaiter(this, void 0, void 0, function* () { return this.currentStep++; }));
        journey.on(AbstractPuppeteerJourney_1.PuppeteerJourneyEvents.JOURNEY_NEW_CONTEXT, () => __awaiter(this, void 0, void 0, function* () { return this.currentContext++; }));
        journey.on(AbstractPuppeteerJourney_1.PuppeteerJourneyEvents.JOURNEY_END, () => __awaiter(this, void 0, void 0, function* () { return this.stopTimer(true); }));
        journey.on(AbstractPuppeteerJourney_1.PuppeteerJourneyEvents.JOURNEY_ERROR, () => __awaiter(this, void 0, void 0, function* () { return this.stopTimer(false); }));
        (_a = this.context) === null || _a === void 0 ? void 0 : _a.eventBus.on(ModuleInterface_1.ModuleEvents.startsComputing, () => this.pauseTimer());
        (_b = this.context) === null || _b === void 0 ? void 0 : _b.eventBus.on(ModuleInterface_1.ModuleEvents.endsComputing, () => this.unpauseTimer());
    }
    /**
     * {@inheritdoc}
     */
    analyse(urlWrapper) {
        var _a, _b, _c, _d, _e;
        this.pauseTimer();
        if (!this.hasValue) {
            return Promise.resolve(false);
        }
        (_a = this.context) === null || _a === void 0 ? void 0 : _a.eventBus.emit(exports.CPUModuleEvents.beforeAnalyse, { module: this, url: urlWrapper });
        (_b = this.context) === null || _b === void 0 ? void 0 : _b.eventBus.emit(ModuleInterface_1.ModuleEvents.beforeAnalyse, { module: this, url: urlWrapper });
        const result = this.getResult(urlWrapper);
        (_c = this.context) === null || _c === void 0 ? void 0 : _c.eventBus.emit(exports.CPUModuleEvents.onResult, { module: this, url: urlWrapper, result: result });
        (_d = this.context) === null || _d === void 0 ? void 0 : _d.eventBus.emit(exports.CPUModuleEvents.afterAnalyse, { module: this, url: urlWrapper, result: result });
        (_e = this.context) === null || _e === void 0 ? void 0 : _e.eventBus.emit(ModuleInterface_1.ModuleEvents.afterAnalyse, { module: this, url: urlWrapper });
        return Promise.resolve((result === null || result === void 0 ? void 0 : result.success) || false);
    }
    /**
     * Start timer
     */
    startTimer(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const cdp = yield data.wrapper.page.target().createCDPSession();
            this.hasValue = false;
            this.currentStep = 0;
            this.currentContext = 0;
            this.snapshots = [];
            this.isPaused = false;
            yield cdp.send('Performance.enable', {
                timeDomain: 'timeTicks',
            });
            this.addMetrics(cdp);
            this.interval = setInterval(() => __awaiter(this, void 0, void 0, function* () {
                if (!this.isPaused) {
                    this.addMetrics(cdp);
                }
            }), 100);
        });
    }
    /**
     * Add metrics.
     *
     * @param cdp
     * @returns {Promise<void>}
     * @protected
     */
    addMetrics(cdp) {
        return __awaiter(this, void 0, void 0, function* () {
            let metrics;
            try {
                metrics = yield cdp.send('Performance.getMetrics');
            }
            catch (err) {
                this.stopTimer(true);
                return;
            }
            this.snapshots.push({
                metrics: this.getMetricsValues(metrics),
                step: this.currentStep,
                context: this.currentContext,
                timestamp: 0,
                usage: 0,
            });
        });
    }
    /**
     * Return metrics from browser.
     *
     * @param metrics
     * @returns {{timestamp: number, activeTime: number}}
     * @protected
     */
    getMetricsValues(metrics) {
        const values = {};
        metrics.metrics
            .forEach((item) => {
            values[item.name] = item.value;
        });
        return values;
    }
    /**
     * Pause timer.
     *
     * @private
     */
    pauseTimer() {
        this.isPaused = true;
    }
    /**
     * Unpause timer.
     *
     * @private
     */
    unpauseTimer() {
        this.isPaused = false;
    }
    /**
     * Stop timer.
     */
    stopTimer(hasValue) {
        this.hasValue = hasValue;
        clearInterval(this.interval);
    }
    /**
     * Return the result.
     *
     * @param {UrlWrapper} urlWrapper
     * @returns {any}
     * @private
     */
    getResult(urlWrapper) {
        this.snapshots.sort((snapA, snapB) => snapA.metrics.Timestamp - snapB.metrics.Timestamp);
        const firstTimeStamp = this.snapshots[0].metrics.Timestamp;
        this.snapshots[0].metrics.Timestamp = 0;
        for (let i = this.snapshots.length - 1; i > 0; i--) {
            this.snapshots[i].timestamp = this.snapshots[i].metrics.Timestamp - firstTimeStamp;
            Object.keys(this.snapshots[i].metrics).forEach((metricName) => {
                this.snapshots[i].metrics[metricName] -= this.snapshots[i - 1].metrics[metricName];
            });
            this.snapshots[i].usage = this.getSnapshotUsage(this.snapshots[i]);
        }
        this.snapshots.forEach((snapshot) => {
            var _a, _b, _c, _d, _e;
            const item = {
                url: urlWrapper.url,
                time: snapshot.timestamp,
                step: ((_a = this.journeySteps[snapshot.step]) === null || _a === void 0 ? void 0 : _a.name) || '',
                context: ((_b = this.journeyContexts[snapshot.context]) === null || _b === void 0 ? void 0 : _b.name) || '',
                cpu: snapshot.usage,
            };
            (_e = (_d = (_c = this.context) === null || _c === void 0 ? void 0 : _c.config) === null || _d === void 0 ? void 0 : _d.storage) === null || _e === void 0 ? void 0 : _e.add('cpu_history', this.context, item);
        });
        this.getAverageData(urlWrapper.url).forEach((average) => {
            var _a, _b, _c, _d, _e;
            (_c = (_b = (_a = this.context) === null || _a === void 0 ? void 0 : _a.config) === null || _b === void 0 ? void 0 : _b.storage) === null || _c === void 0 ? void 0 : _c.add('cpu', this.context, average);
            (_e = (_d = this.context) === null || _d === void 0 ? void 0 : _d.config) === null || _e === void 0 ? void 0 : _e.logger.result('CPU', average, urlWrapper.url.toString());
        });
        this.unpauseTimer();
        return true;
    }
    /**
     * Return average data.
     *
     * @returns {{cpu: number, time: any}}
     * @private
     */
    getAverageData(url) {
        const averages = [];
        this.journeyContexts.forEach((context, index) => {
            const contextStocks = this.snapshots.filter((item) => item.context === index);
            const time = contextStocks[contextStocks.length - 1].timestamp - contextStocks[0].timestamp;
            const cumulActiveTime = contextStocks.reduce((sum, currentValue) => {
                if (currentValue === null || currentValue === void 0 ? void 0 : currentValue.usage) {
                    return sum + currentValue.usage;
                }
                return sum;
            }, 0);
            averages.push({
                cpu: cumulActiveTime / contextStocks.length,
                time: time,
                context: context.name,
                url: url,
            });
        });
        return averages;
    }
    /**
     * Return snappshot usage value.
     *
     * @param {CPUUsageSnapshot} snapshot
     * @returns {number}
     * @private
     */
    getSnapshotUsage(snapshot) {
        let all = 0;
        this.durationsMetrics.forEach((metricName) => {
            all += snapshot.metrics[metricName];
        });
        // The all time is the cumulation of all time of all cpus used during the snapshot session.
        return all / this.cpuCount / snapshot.metrics.Timestamp * 100;
    }
}
exports.CPUModule = CPUModule;
