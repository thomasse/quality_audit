"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModuleEvents = void 0;
exports.ModuleEvents = {
    beforeUrlProcess: 'beforeURLProcess',
    afterUrlProcess: 'afterURLProcess',
    beforeAnalyse: 'beforeAnalyse',
    afterAnalyse: 'afterAnalyse',
    onAnalyseResult: 'onAnalyseResult',
    startsComputing: 'startsComputing',
    endsComputing: 'endsComputing',
};
