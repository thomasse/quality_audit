"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UrlWrapper = void 0;
/**
 * Url wrapper object.
 */
class UrlWrapper {
    constructor(url, data) {
        this.url = url;
        this.data = data;
    }
}
exports.UrlWrapper = UrlWrapper;
