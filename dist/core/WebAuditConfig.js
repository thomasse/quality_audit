"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebAuditConfigClass = void 0;
/**
 * Config.
 */
class WebAuditConfigClass {
    constructor(logger, storage) {
        this.logger = logger;
        this.storage = storage;
    }
}
exports.WebAuditConfigClass = WebAuditConfigClass;
