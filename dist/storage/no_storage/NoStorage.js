"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * store data in
 */
class NoStorage {
    /**
     * Init Store.
     */
    installStore(id, context, data) {
    }
    /**
     * Add data to Store
     *
     * @param id
     * @param context
     * @param data
     */
    add(id, context, data) {
    }
    /**
     * Replace data.
     */
    one(id, context, data) {
    }
    /**
     * Add file.
     *
     * @param {string} input
     * @param context
     */
    file(input, context) {
    }
}
exports.default = NoStorage;
