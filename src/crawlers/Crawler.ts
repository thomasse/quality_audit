import {HTTPResponse} from 'puppeteer';

import {UrlWrapper} from '../core/UrlWrapper';
import {PageWrapper} from '../journey/PageWrapper';
import {JourneyInterface} from '../journey/JourneyInterface';
import {WebAuditContextClass} from '../core/WebAuditContext';

export interface WebAuditCrawlerType {
  baseUrl: URL;
  domain?: URL;
  crawlerOptions?: any;
  allowedStatus?: number[];
  followSearchParams?: boolean;
  isEligibleUrl?: Function;
  uniqueParams?: string[];
}

/**
 * Events.
 *
 * @type {{onCreateCrawl: string}}
 */
export const WebAuditCrawlerEvents: any = {
  createCrawl: 'crawler__createCrawl',
  beforeCrawl: 'crawler__beforeCrawl',
  afterCrawl: 'crawler__afterCrawl',
  onCrawlUrls: 'crawler__onCrawlUrls',
  onPageCrawled: 'crawler__onPageCrawled',
  onPageCrawledError: 'crawler__onPageCrawledError',
  onPageCrawledBadStatus: 'crawler__onPageCrawledBadStatus',
  onPageCrawledNoUri: 'crawler__onPageCrawledNoUri',
  onPageCrawledRedirected: 'crawler__onPageCrawledRedirected',
  onPageContent: 'crawler__onPageContent',
  onCrawlUrlsEnd: 'crawler__onCrawlUrlsEnd',
};

/**
 * Website crawler.
 */
export class WebAuditCrawler {

  protected defaultOptions: any = {
    followSearchParams: true,
    uniqueParams: ['page'],
  };

  private options: WebAuditCrawlerType;

  private urlsToParse: any = {};

  private alreadyParsedUrls: string[] = [];

  private pageWrapper: PageWrapper;

  /**
   * Constructor.
   *
   * @param eventEmitter
   * @param baseUrlWrapper
   * @param options
   */
  constructor(
    protected context: WebAuditContextClass,
    public baseUrlWrapper: UrlWrapper,
    options?: WebAuditCrawlerType,
  ) {
    this.options = {
      ...this.defaultOptions,
      ...options,
    };
    this.pageWrapper = new PageWrapper(this.context);

    // Prepare options.
    this.initBaseUrl(baseUrlWrapper.url.toString());

    // Emit.
    this.context.eventBus.emit(WebAuditCrawlerEvents.createCrawl, {crawler: this, baseUrl: this.baseUrlWrapper});

    // Prepare storage.
    this.context.config.storage?.installStore(
      'page_found',
      this.context,
      {
        url: 'Referenced url',
        status: `Status`,
        size: `Content length`,
        final: 'Final URL (if redirected)',
        source: `Orignal page (where url is referenced)`,
      },
    );
  }

  /**
   * Crawl url.
   */
  async crawl(journey: JourneyInterface): Promise<void> {
    // Init puppeteer browser.
    await this.pageWrapper.newPage();

    await journey.beforeAll(this.pageWrapper, [this.baseUrlWrapper]);

    await this.crawlUrl(this.baseUrlWrapper.url, null, journey);

    await this.pageWrapper.close();
  }

  /**
   * Crawl url.
   *
   * @param {UrlWrapper} url
   * @private
   */
  private async crawlUrl(url: URL, source: URL | null = null, journey: JourneyInterface): Promise<void> {
    if (this.isAlreadyParsed(url)) {
      return Promise.resolve();
    }

    this.context.setData('Page crawled').setUrl(url);

    // Get info.
    let pageInfo: any;
    try {
      pageInfo = await this.getPageInfo(this.pageWrapper, url, source, journey);
    } catch (error) {
      return Promise.resolve();
    }

    const eventData = {crawler: this, data: pageInfo, baseUrl: this.baseUrlWrapper, pageWrapper: this.pageWrapper};

    // Add to parsed urls.
    this.addToParsedUrls(pageInfo.url);
    this.addToParsedUrls(pageInfo.final);
    this.addToParsedUrls(pageInfo.source);

    if (pageInfo.log) {
      this.context.eventBus.emit(WebAuditCrawlerEvents.onPageCrawled, eventData);
      this.context.config.storage?.add('page_found', this.context, pageInfo);

      if (pageInfo.status >= 300 && pageInfo < 400) {
        this.context.eventBus.emit(WebAuditCrawlerEvents.onPageCrawledRedirected, eventData);
      }

      if (pageInfo.crawl) {
        await this.crawlSubPages(this.pageWrapper, pageInfo.final, journey);
      }
    }

    return Promise.resolve();
  }

  /**
   * Return page info.
   *
   * @param {PageWrapper} pageWapper
   * @param {UrlWrapper} inputUrl
   * @param {UrlWrapper | null} source
   * @returns {Promise<any>}
   * @private
   */
  private async getPageInfo(pageWapper: PageWrapper, inputUrl: URL, source: URL | null, journey: JourneyInterface): Promise<any> {
    const infos: any = {
      url: inputUrl.toString(),
      source: source?.toString() || '',
      status: '',
      size: '',
      final: '',
      crawl: true,
      log: true,
    };

    // Check eligibility.
    const beforeCrawl = await journey.isEligible(pageWapper, new UrlWrapper(inputUrl));
    if (!beforeCrawl) {
      infos.crawl = false;
      infos.log = false;
      return infos;
    }

    this.context.config.logger.message(`Parse : ${inputUrl.toString()}`);

    // Listen data.
    let status: number | string = '';
    let size: number | string = '';
    const onResponse = async (response: HTTPResponse) => {
      if (status === '') {
        // eslint-disable-next-line require-atomic-updates
        status = await response.status();
      }

      if (size === '') {
        try {
          // eslint-disable-next-line require-atomic-updates
          size = (await response.buffer()).length;
        } catch (error) {
          // Redirect has no size.
        }
      }
    };

    this.pageWrapper.page.on('response', onResponse);

    // Navigate to page.
    try {
      await this.pageWrapper.goto(inputUrl.toString(), true);
    } catch (error) {
      this.context.config.logger.error(error);
      this.pageWrapper.page.off('response', onResponse);
      return Promise.resolve(infos);
    }

    try {
      await this.pageWrapper.page.waitForSelector('body');
    } catch (err) {
      this.context.config.logger.error(`Load timeout`);
    }

    // Remove listeneer data.
    this.pageWrapper.page.off('response', onResponse);


    infos.status = status;
    infos.size = size;
    infos.final = await pageWapper.page.url() || '';
    return infos;
  }


  /**
   * Crawl inner href.
   *
   * @param {PageWrapper} pageWrapper
   * @param {URL} source
   * @param journey
   * @returns {Promise<void>}
   * @private
   */
  private async crawlSubPages(pageWrapper: PageWrapper, source: URL, journey: JourneyInterface) {
    // Get
    const hrefs: URL[] = [];
    const links = await pageWrapper.page.$$('a[href], link[rel="alternate"]');

    // Clean href links.
    for (const link of links) {
      try {
        const href = await (await link.getProperty('href')).jsonValue();
        const cleanURL = this.getCleanUrlFromHref(href, source);
        if (cleanURL) {
          hrefs.push(cleanURL);
        }
      } catch (error) {
        // Bad URL.
      }
    }

    const subUrls = this.getEligibleUrls(hrefs);
    this.context.eventBus.emit(WebAuditCrawlerEvents.onCrawlUrls, {
      crawler: this,
      urlsList: subUrls,
      baseUrl: this.baseUrlWrapper,
    });
    for (const url of subUrls) {
      await this.crawlUrl(url, source, journey);
    }
  }

  /**
   * Clean base url.
   *
   * @private
   */
  private initBaseUrl(url: string) {
    try {
      this.options.baseUrl = new URL(url);

      // define domain
      this.options.domain = new URL(url);
      this.options.domain.hash = '';
      this.options.domain.pathname = '';
      this.options.domain.search = '';
    } catch (erro) {
      this.context.config.logger.exit(`Base URL is not of type URL`);
    }
  }

  /**
   * Parse only domain url.
   *
   * @param url
   * @private
   */
  private isDomainUrl(url: URL): boolean {
    return url.host === this.options.baseUrl.host;
  }

  /**
   * Return true if url is already queued.
   *
   * @param url
   * @private
   */
  private isAlreadyAddedToQueue(url: URL) {
    return typeof this.urlsToParse[this.normalizeURL(url)] !== 'undefined';
  }

  /**
   * Return only crawl eligible urls.
   *
   * @param urls
   * @private
   */
  private getEligibleUrls(urls: URL[]) {
    let eligibleUrls: URL[] = urls.filter((url) => {
      return (
        !this.isAlreadyAddedToQueue(url) &&
        !this.isAlreadyParsed(url) &&
        this.isDomainUrl(url) &&
        // this.isHtmlUrl(url) &&
        this.isUserEligible(url)
      );
    });

    if (eligibleUrls.length > 1) {
      eligibleUrls = eligibleUrls
        .map((url: URL) => new URL(`${url.protocol}//${this.normalizeURL(url)}`))
        .filter((url: URL) => url);
      eligibleUrls = this.uniqueUrls(eligibleUrls);
    }

    return eligibleUrls;
  }

  /**
   * Test if url is already parsed.
   *
   * @param {URL} url
   * @returns {boolean}
   * @private
   */
  private isAlreadyParsed(url: URL) {
    return this.alreadyParsedUrls.indexOf(this.normalizeURL(url)) > -1;
  }

  /**
   * Add url to already parsed.
   *
   * @param {URL} url
   * @private
   */
  private addToParsedUrls(url: URL | null) {
    if (url && !this.isAlreadyParsed(url)) {
      this.alreadyParsedUrls.push(this.normalizeURL(url));
    }
  }

  /**
   * Return true if url is eligible from user callback.
   *
   * @param url
   * @private
   */
  private isUserEligible(url: URL) {
    return this.options.isEligibleUrl ? this.options.isEligibleUrl(url, this) : true;
  }

  /**
   * To readable urls.
   *
   * @param urls
   * @private
   */
  private readable(urls: URL[]) {
    return urls.map((url) => url.toString());
  }

  /**
   * Unique urls.
   *
   * @param urls
   * @private
   */
  private uniqueUrls(urls: URL[]) {
    const count: any = {};
    return urls.filter((url) => {
      const str = url.toString();
      count[str] = count[str] ? count[str] + 1 : 1;
      return count[str] < 2;
    });
  }

  /**
   * Return clea url from href.
   *
   * @param href
   * @param origin
   * @private
   */
  private getCleanUrlFromHref(href: string, origin: URL | null) {
    let input = href;

    // Deal with anchor.
    if (input.indexOf('#') === 0) {
      return null;
    }

    // Deal with relative href.
    if (input.indexOf('/') === 0 && input.length > 1) {
      input = `${this.options.domain?.toString()}${input}`;
    }

    // Deal with parameters urls.
    if (this.options.followSearchParams && input.indexOf('?') === 0) {
      if (origin && input.length > 1) {
        const url = new URL(origin);
        url.search = input;
        input = url.toString();
      } else {
        return null;
      }
    }

    const url = new URL(input.replace(/\/\//g, '/'));

    // Check user eligibility.
    if (!this.options.followSearchParams) {
      url.search = '';
    }

    return url;
  }

  /**
   * Normalise url
   *
   * @param {URL} url
   * @returns {string}
   */
  private normalizeURL(url: URL): string {
    const idURL: URL = new URL(url);
    idURL.hash = '';
    idURL.protocol = '';

    if (!this.options.followSearchParams) {
      idURL.search = '';
    }

    const uniqueParams: string[] = this.options.uniqueParams || [];
    if (this.options?.uniqueParams?.length) {
      Array.from(idURL.searchParams)
        .filter(([key]) => uniqueParams.indexOf(key) < 0)
        .forEach(([key]) => {
          idURL.searchParams.delete(key);
        });
    }

    // Delete protocole.
    let id: string = idURL
      .toString()
      .replace(`${idURL.protocol}//`, '');

    // Delete //.
    while (id.indexOf('//') > -1) {
      id = id.replace(/\/\//g, '/');
    }

    // Delete last /.
    while (id[id.length - 1] === '/') {
      id = id.slice(0, -1);
    }

    return id;
  }
}
