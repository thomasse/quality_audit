/**
 * Url wrapper object.
 */
export class UrlWrapper {
  constructor(
    public url: URL,
    public data?: any,
  ) {
  }
}
