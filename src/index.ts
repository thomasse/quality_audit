import {WebAuditConfigClass} from './core/WebAuditConfig';
import {WebAuditCoreClass} from './core/WebAuditCore';
import {WebAuditContextClass} from './core/WebAuditContext';
import {WebAuditEventClass} from './core/WebAuditEvent';

export const Config = WebAuditConfigClass;
export const Context = WebAuditContextClass;
export const Core = WebAuditCoreClass;
export const Event = WebAuditEventClass;
